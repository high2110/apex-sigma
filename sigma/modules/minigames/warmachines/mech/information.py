# Apex Sigma: The Database Giant Discord Bot.
# Copyright (C) 2018  Lucia's Cipher
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

manufacturers = {
    0: 'Lux',  # Vector
    1: 'Aurora',  # Lee-Enfield
    2: 'Kitsnik',  # G41
    3: 'Oflinn',  # Negev
    4: 'Valure',  # Contender
    5: 'Gargaron',  # OTs-14
    6: 'Shaflon',  # M870
    7: 'Merbuhl'  # Five-seveN
}

manu_bases = {
    0: {
        'health': 93,
        'damage': 12,
        'accuracy': 2,
        'evasion': 10,
        'rate_of_fire': 71,
        'crit_chance': 5,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    1: {
        'health': 40,
        'damage': 45,
        'accuracy': 9,
        'evasion': 5,
        'rate_of_fire': 23,
        'crit_chance': 40,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    2: {
        'health': 64,
        'damage': 18,
        'accuracy': 5,
        'evasion': 5,
        'rate_of_fire': 52,
        'crit_chance': 20,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    3: {
        'health': 87,
        'damage': 28,
        'accuracy': 4,
        'evasion': 4,
        'rate_of_fire': 90,
        'crit_chance': 5,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    4: {
        'health': 33,
        'damage': 14,
        'accuracy': 9,
        'evasion': 9,
        'rate_of_fire': 27,
        'crit_chance': 40,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    5: {
        'health': 55,
        'damage': 17,
        'accuracy': 7,
        'evasion': 7,
        'rate_of_fire': 50,
        'crit_chance': 20,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    },
    6: {
        'health': 132,
        'damage': 13,
        'accuracy': 2,
        'evasion': 2,
        'rate_of_fire': 20,
        'crit_chance': 40,
        'crit_damage': 50,
        'amor': 3,
        'amor_pen': 10
    },
    7: {
        'health': 32,
        'damage': 11,
        'accuracy': 7,
        'evasion': 12,
        'rate_of_fire': 44,
        'crit_chance': 20,
        'crit_damage': 50,
        'amor': 0,
        'amor_pen': 10
    }
}
